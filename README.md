# Here we collect our best practicies for making MIP problem.

We support feature oriented programming, where mathematical model consists from set of features each can be turned on/off or tuned separately. 

We think that MIP model should consist from features and not from variables, constraints and objective function.

So code of some feature should not splitted into three different parts.
Search space (variables) should be construted in the same time with constraints and obj.
It helps minimize search space, help make mip model more debuggable   and flexible
 i.e. it helps move space definition (variables) to hard constraints, and hard contraints into soft contraints and vice versa. 


Currenlty in the project we have two ideas: 
1) objective function constructor, which allows to add block to objective functions 
2) constraints constructor from dictionary of constraints. It helps to generate non-rectangle set of constraints with debuggable names. 