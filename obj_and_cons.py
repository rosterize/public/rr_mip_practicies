from pyomo.environ import (value, ConstraintList, Constraint)
from collections import defaultdict

import logging
import structlog

logger = structlog.getLogger('rr_mip_practicies')

def constraints_from_dict(cons, model, prefix):
    if type(cons) is dict:
        if not cons:
            return
        def rule(model, *k):
            if len(k) == 1:
                k = k[0]
            ret = cons[k]
            if ret is True:
                return Constraint.Feasible
            return ret
        result = Constraint(cons.keys(), rule=rule)
        setattr(model, prefix, result)
    elif type(cons) is list:
        if not cons:
            return
        result = ConstraintList()
        setattr(model, prefix, result)
        for c in cons:
            if not c is None :
                result.add(c)
    else:
        result = Constraint(expr=cons)
        setattr(model, prefix, result)


class ObjectiveComponents(object):
    def __init__(self, model):
        self.model = model
        self.descr = {} ## for not all keys var will exist
        self.expr = {} ## for not all keys var will exist
        self.groups = defaultdict(list)


    def add(self, path, varname, key, penalty, **params):
        self.groups[path].append((varname, key))

        params['var_key'] = key
        params['path']=path + '.' + varname
        params['base_penalty']=penalty
                
        params['group']=path.split('.')[0]
        self.descr[(path, varname, key)] = params 

    def del_var(self, path, varname):
        self.descr = {(p,v,k): params for (p,v,k),params in self.descr.items() if p!=path and v!=varname}

    def add_expr(self, path, expr, exprkey, penalty, **params):
        self.groups[path].append(("expr", exprkey))
        
        params['var_key'] = exprkey
        params['path']=path
        params['base_penalty']=penalty

        params['group']=path.split('.')[0]
        self.descr[(path, "expr", exprkey)] = params 
        self.expr[(path, exprkey)] = expr

    def show(self, path, varname, key):

        p= self.descr[(path, varname, key)]
        var = getattr(self.model,varname)
        p["value"] = value(var[key])
        p["penalty"] =p["base_penalty"] * p["value"]
        return p

    def sumprefix(self, prefix=""):
        s=0
        for (path, varname, key) in self.descr :
            if path.startswith(prefix) :
                p= self.descr[(path, varname, key)]
                if varname == 'expr' :
                    s  += p["base_penalty"] * value(self.expr[path, key])
                elif hasattr(self.model, varname) :
                    var = getattr(self.model, varname)
                    if key in var and not (value(var[key]) is None) and abs(value(var[key])) > 1e-6 :
                        s +=   p["base_penalty"] * value(var[key])
        return s

    def iter_prefix(self, prefix=""):
        for (path, varname, key) in self.descr :
            if path.startswith(prefix) :
                p= self.descr[(path, varname, key)]
                if varname == 'expr' :
                    yield path, varname, key, p["base_penalty"], value(self.expr[path, key])
                elif hasattr(self.model, varname) :
                    var = getattr(self.model, varname)
                    if key in var and not (value(var[key]) is None):
                        yield path, varname, key, p["base_penalty"], value(var[key])

    def showall(self):
        result=[]
        for (path, varname, key) in self.descr :
            p = None
            if varname == 'expr' :
                if not (value(self.expr[path, key]) is None) and value(self.expr[path, key]) > 1e-6:
                    p= dict(self.descr[(path, varname, key)])
                    p["value"] = value(self.expr[path, key])
            elif hasattr(self.model, varname) :
                var = getattr(self.model, varname)
                if key in var and not (value(var[key]) is None) and abs(value(var[key])) > 1e-6 :
                    p= dict(self.descr[(path, varname, key)])
                    p["value"] = value(var[key])

            if p is None:
                continue
            p["penalty"] =p["base_penalty"] * p["value"]
            if abs(p["penalty"]) >1e-6:
                result.append(p)
        result.sort(key = lambda x: x.get("path"))
        return result

    def build(self, min_base_penalty= 0, skipvars=[]):
        obj = []
        quicksum = sum

        for (path, varname, key) in self.descr :
            if varname == 'expr':
                if min_base_penalty == 0:
                    obj.append(self.expr[path, key] * self.descr[ (path, varname, key) ]["base_penalty"])
            elif hasattr(self.model, varname) and varname not in skipvars:
                var = getattr(self.model, varname)
                if key in var :
                    if abs(self.descr[ (path, varname, key) ]["base_penalty"]) > min_base_penalty :
                        obj.append(var[key] * self.descr[ (path, varname, key) ]["base_penalty"])
        logger.info("built obj from  %s elements", len(obj) )
        
        return quicksum(obj)
